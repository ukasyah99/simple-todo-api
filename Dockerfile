# syntax=docker/dockerfile:1
FROM 046114766704.dkr.ecr.ap-southeast-1.amazonaws.com/golang:1.17.3-alpine AS builder
WORKDIR /app
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN go build -o bin/run

FROM 046114766704.dkr.ecr.ap-southeast-1.amazonaws.com/alpine:3.13.6
COPY --from=builder /app/bin/run .
EXPOSE 8080
ENTRYPOINT ["./run"]
